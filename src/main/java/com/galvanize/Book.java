package com.galvanize;

public class Book {

    private String bookTitle;
    private int bookYear;
    private Author author;
    private Publisher publisher;

    public String getTitle() {
        return bookTitle;
    }

    public void setTitle(String bookName) {
        this.bookTitle = bookName;
    }

    public int getYear() {
        return bookYear;
    }

    public void setYear(int bookYear) {
        this.bookYear = bookYear;
    }

    public String getAuthorFirstName() {
        return author.getFirstName();
    }

    public void setAuthorFirstName(String authorFirstName) {
        author.setFirstName(authorFirstName);
    }

    public String getAuthorLastName() {
        return author.getLastName();
    }

    public void setAuthorLastName(String authorLastName) {
        author.setLastName(authorLastName);
    }

    public String getPublisherName() {
        return publisher.getName();
    }

    public Author getAuthor() {
        return author;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public void setPublisherName(String publisherName) {
        publisher.setName(publisherName);
    }

    public String getPublisherCity() {
        return publisher.getCity();
    }

    public void setPublisherCity(String publisherCity) {
        publisher.setCity(publisherCity);
    }

    public String getFormattedPublisherName() {
        return publisher.getFormattedName();
    }

    public String getFormattedAuthorName() {
        return author.getFormattedName();
    }

    public String getFormattedName() {
        return String.format(
                "%s (%s)\nWritten by %s\nPublished by %s",
                getTitle(),
                getYear(),
                getFormattedAuthorName(),
                getFormattedPublisherName()
        );
    }
}
